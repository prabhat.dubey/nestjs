import {
    Body,
    Controller,
    Get,
    HttpCode,
    HttpStatus,
    Query,
    Post,
} from '@nestjs/common';
import { CreateFormDTO, GetFormPathParams } from '../dto/form.dto';
import { FormService } from '../service/form.service';
import { Response as ResponseCustom } from 'src/utils/response/response.decorator';
import { responseName } from 'src/constants/response';
import { FormFieldService } from '../service/form.field.service';
import { FormDataService } from '../service/form.data.service';
@Controller()
export class FormController {
    constructor(private formService: FormService, private formFieldService: FormFieldService, private formDataService: FormDataService) {}


    // POST  API to create the form.
    @Post()
    @HttpCode(HttpStatus.CREATED)
    @ResponseCustom(responseName.FORM_CREATED)
    async createForm(@Body() createFormDTO: CreateFormDTO) {
        let form = await this.formService.create(createFormDTO);
        const { title, ...formField } = createFormDTO;
        if(form){
            const fieldEntries = Object.entries(formField);
            let fieldAllEntities = [];
            fieldAllEntities = fieldEntries.map(([fieldName, fieldType]) => ({
                fieldName,
                fieldType,
                formId: form.id
            }));
           this.formFieldService.createFormFields(fieldAllEntities);
        }
        return form;
    }

    // GET API to get the form by title.
    @Get('/fill_data')
    @ResponseCustom(responseName.GET_FORM)
    @HttpCode(HttpStatus.OK)
    async getFormtByTitle(@Query('form_title') formTitle: string) {
        return await this.formService.findByTitle(formTitle);
    }

     // POST API to save the form data.
     @Post('/fill_data')
     @ResponseCustom(responseName.DATA_CREATED)
     @HttpCode(HttpStatus.CREATED)
     async saveFormData(@Body() data : any) {
         return await this.formDataService.create(data);
     }

}
