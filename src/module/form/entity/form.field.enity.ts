import {
  Model,
  Column,
  DataType,
  Table,
  ForeignKey,
} from 'sequelize-typescript';
import { FormFieldDTO } from '../dto/form.field.dto';
import { FormEntity } from './form.entity';

@Table({ tableName: 'formField' })
export class FormFieldEntity extends Model<FormFieldDTO> {
  @Column({
    primaryKey: true,
    type: DataType.INTEGER,
    autoIncrement: true,
    allowNull: false,
  })
  id: number;

  @Column({
    allowNull: false,
    type: DataType.INTEGER,
    defaultValue: DataType.INTEGER,
  })
  @ForeignKey(() => FormEntity)
  formId: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  fieldName: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  fieldType: string;
}
