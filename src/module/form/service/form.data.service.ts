import { Injectable } from '@nestjs/common';
import { FormDataRepository } from '../repository/form.data.repository';
import { CreateDataDTO } from '../dto/form.data.dto';

@Injectable()
export class FormDataService {
    constructor(
        private formDataRepository: FormDataRepository,
    ) {}

    async create(createDataDTO: any) {

        const { formId, ...dataField } = createDataDTO;
        
            const dataEntries = Object.entries(dataField);
            let dataAllEntities = [];
            dataAllEntities = dataEntries.map(([formFieldId, value]) => ({
                formFieldId,
                value,
                formId:formId
            }));
        const form = await this.formDataRepository.create(dataAllEntities);
        return form;
    }
}
