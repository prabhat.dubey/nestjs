import { ApiProperty, OmitType } from '@nestjs/swagger';
import { IsBoolean, IsNotEmpty, IsNumber, IsString, IsUUID } from 'class-validator';

export class FormDTO {
    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    id: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsUUID(4)
    uniqueId: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    title: string;
}

export class CreateFormDTO extends OmitType(FormDTO, ['id','uniqueId'] as const) {}

export class GetFormPathParams {
    @IsNotEmpty()
    @IsUUID(4)
    id: string;
}
