import { Injectable } from '@nestjs/common';
import { FormFieldDTO } from '../dto/form.field.dto';
import { FormFieldRepository } from '../repository/form.field.repository';
@Injectable()
export class FormFieldService {
    constructor(
        private formFieldRepository: FormFieldRepository,
    ) {}

//     async createFields(user: any) {
//         const fieldEntries = Object.entries(user);
//         const fieldAllEntities = fieldEntries.map(([fieldName, fieldType]) => ({
//           fieldName,
//           fieldType,
//     }));
//     return fieldAllEntities;

//    }

    async createFormFields(createformDTO: FormFieldDTO[]) {
        const form = await this.formFieldRepository.create(createformDTO);
        return { form };
    }
   
}
