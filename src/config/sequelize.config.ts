import { SequelizeModuleOptions } from '@nestjs/sequelize';

const sequelizeConfig: SequelizeModuleOptions = {
  dialect: 'mysql',
  // host: process.env.DB_HOST,
  // port: parseInt(process.env.DB_PORT),
  // username: process.env.DB_USERNAME,
  // password: process.env.DB_PASSWORD,
  // database: process.env.DB_DATABASE,
  host: '10.102.32.196',
  port: 3306,
  username: 'development',
  password: 'Admin@123',
  database: 'nestDB',
  autoLoadModels: true, // Automatically load models from defined directories
  synchronize: true, // Sync models with database (should be false in production)
};

export default sequelizeConfig;
