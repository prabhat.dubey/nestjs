import { ApiProperty, OmitType } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class FormFieldDTO {
    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    id: number;

    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    formId: number;

    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    fieldName: number;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    fieldType: string;
}

export class CreateFormDTO extends OmitType(FormFieldDTO, ['id'] as const) {}

