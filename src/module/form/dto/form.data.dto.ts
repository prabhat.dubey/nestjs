import { ApiProperty, OmitType } from '@nestjs/swagger';
import { IsNotEmpty, IsNumber, IsString } from 'class-validator';

export class FormDataDTO {
    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    id: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    formId: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsNumber()
    formFieldId: string;

    @ApiProperty()
    @IsNotEmpty()
    @IsString()
    value: string;
}

export class CreateDataDTO extends OmitType(FormDataDTO, ['id'] as const) {}

