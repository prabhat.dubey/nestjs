import { Injectable } from '@nestjs/common';
import { FormRepository } from '../repository/form.repository';
import { CreateFormDTO } from '../dto/form.dto';

@Injectable()
export class FormService {
    constructor(
        private formRepository: FormRepository,
    ) {}

    async create(createformDTO: CreateFormDTO) {
        const form = await this.formRepository.create(createformDTO);
        return form;
    }

    async findByTitle(title: string) {
       return await this.formRepository.findAll(title);
    }
   
}
