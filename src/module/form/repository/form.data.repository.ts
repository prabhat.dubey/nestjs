import { Injectable } from '@nestjs/common';
import { CreateFormDTO } from '../dto/form.dto';
import { FormDataEntity } from '../entity/form.data.entity';
import {  CreateDataDTO } from '../dto/form.data.dto';
@Injectable()
export class FormDataRepository {
    async create(createDataDTO: CreateDataDTO[]) {
        return await FormDataEntity.bulkCreate(createDataDTO);
    }

}
