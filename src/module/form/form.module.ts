import { Module } from '@nestjs/common';
import { FormController } from './controller/form.controller';
import { FormService } from './service/form.service';
import { FormFieldService } from './service/form.field.service';
import { FormDataService } from './service/form.data.service';
import { FormRepository } from './repository/form.repository';
import { FormFieldRepository } from './repository/form.field.repository';
import { FormDataRepository } from './repository/form.data.repository';

@Module({

  imports: [],
  controllers: [FormController],
  exports: [FormRepository, FormFieldRepository, FormDataRepository],
  providers: [FormRepository, FormFieldRepository,FormDataRepository, FormService, FormFieldService, FormDataService],
 
})
export class FormModule {}
