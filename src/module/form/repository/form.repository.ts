import { Injectable } from '@nestjs/common';
import { CreateFormDTO } from '../dto/form.dto';
import { FormEntity } from '../entity/form.entity';
import { FormFieldEntity } from '../entity/form.field.enity';
@Injectable()
export class FormRepository {
    async create(createFormDTO: CreateFormDTO) {
        return await FormEntity.create(createFormDTO);
    }

    async findAll(title : string) {
        return await FormEntity.findAll({
            where: { title },
            include: [
                {
                    model: FormFieldEntity,
                    as: 'fields' // Assuming the alias for FormFieldEntity is 'fields'
                }
            ]
        });
    }
}
