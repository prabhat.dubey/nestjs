import { Injectable } from '@nestjs/common';
import { FormFieldDTO } from '../dto/form.field.dto';
import { FormFieldEntity } from '../entity/form.field.enity';

@Injectable()
export class FormFieldRepository {
    async create(createFormDTO: FormFieldDTO[]) {
        return await FormFieldEntity.bulkCreate(createFormDTO);
    }
}
