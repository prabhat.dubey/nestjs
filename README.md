# NestJS



## Getting started

To make it easy for you to get started with GitLab, here's a list of recommended next steps.

Already a pro? Just edit this README.md and make it your own. Want to make it easy? [Use the template at the bottom](#editing-this-readme)!

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/prabhat.dubey/nestjs.git
git branch -M main
git push -uf origin main
```

## Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/prabhat.dubey/nestjs/-/settings/integrations)

## Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Set auto-merge](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing (SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

## Form API Documentation
This document outlines the endpoints and data structures for creating, filling, and retrieving form data using the API.

## Create Form
Route: POST /form
Request Body:
json
{
  "uniqueld": "suuid",
  "title": "string",
  "name": "string",
  "email": "string",
  "phoneNumber": "number",
  "isGraduate": "boolean"
}
uniqueld: Unique identifier for the form.
title: Title of the form.
name: Name of the user.
email: Email of the user.
phoneNumber: Phone number of the user.
isGraduate: Boolean indicating whether the user is a graduate.
## Fill Data
Route: POST /fill_data?form_title=<form_title>
Request Body:
json
{
  "uniqueld": "BCC05491-2035-4634-a4ba-4b9b62667475",
  "name": "Test User",
  "email": "test@yopmail.com",
  "phoneNumber": 9876541230,
  "isGraduate": true
}
uniqueld: Unique identifier for the form (from the create form API).
name: Name of the user.
email: Email of the user.
phoneNumber: Phone number of the user.
isGraduate: Boolean indicating whether the user is a graduate.
## Get All Data
Route: GET /fill_data?form_title=<form_title>
Response: Array of responses that are submitted.
Request Validation
Ensure that every key in the request is as per the type defined in the create form API. For example:

uniqueld should be a string.
title should be a string.
name should be a string.
email should be a string.
phoneNumber should be a number.
isGraduate should be a boolean.
## Example Usage:
# Creating a Form:

curl -X POST \
  -H "Content-Type: application/json" \
  -d '{
        "uniqueld": "suuid",
        "title": "Example Form",
        "name": "string",
        "email": "string",
        "phoneNumber": "number",
        "isGraduate": "boolean"
      }' \
  http://your-api-url/form
# Filling Data:

curl -X POST \
  -H "Content-Type: application/json" \
  -d '{
        "uniqueld": "BCC05491-2035-4634-a4ba-4b9b62667475",
        "name": "Test User",
        "email": "test@yopmail.com",
        "phoneNumber": 9876541230,
        "isGraduate": true
      }' \
  http://your-api-url/fill_data?form_title=Example+Form
# Retrieving Data:

curl -X GET http://your-api-url/fill_data?form_title=Example+Form
Feel free to replace http://your-api-url with your actual API endpoint.

