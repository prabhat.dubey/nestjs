import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppRouterModule } from './app.router';
import { SequelizeModule } from '@nestjs/sequelize';
import sequelizeConfig from 'src/config/sequelize.config';
import { FormEntity } from 'src/module/form/entity/form.entity';
import { FormFieldEntity } from 'src/module/form/entity/form.field.enity';
import { FormDataEntity } from 'src/module/form/entity/form.data.entity';
@Module({
    imports: [
        AppRouterModule.register(),
        SequelizeModule.forRoot(sequelizeConfig), 
        SequelizeModule.forFeature([FormEntity, FormFieldEntity, FormDataEntity])
    ],
    controllers: [AppController],
})
export class AppModule {}
