import { Model, Column, DataType, Table, HasMany } from 'sequelize-typescript';
import { FormDTO } from '../dto/form.dto';
import { FormFieldEntity } from './form.field.enity';

@Table({ tableName: 'form' })
export class FormEntity extends Model<FormDTO> {

  @Column({
    primaryKey: true,
    type: DataType.INTEGER,
    autoIncrement: true,
    allowNull: false,
  })
  id: number;

  @Column({
    type: DataType.UUID,
    defaultValue: DataType.UUIDV4,
    allowNull: false,
  })
  uniqueId: string;

  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  title: string;

  @HasMany(() => FormFieldEntity)
  fields: FormFieldEntity[];
}
