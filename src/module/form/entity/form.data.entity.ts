import {
  Model,
  Column,
  DataType,
  Table,
  ForeignKey,
} from 'sequelize-typescript';
import { FormDataDTO } from '../dto/form.data.dto';
import { FormEntity } from './form.entity';
import { FormFieldEntity } from './form.field.enity';
@Table({ tableName: 'formData' })
export class FormDataEntity extends Model<FormDataDTO> {
  @Column({
    primaryKey: true,
    type: DataType.INTEGER,
    autoIncrement: true,
    allowNull: false,
  })
  id: number;

  @Column({
    allowNull: false,
    type: DataType.INTEGER,
    defaultValue: DataType.INTEGER,
  })
  @ForeignKey(() => FormEntity)
  formId: string;

  @Column({
    allowNull: false,
    type: DataType.INTEGER,
    defaultValue: DataType.INTEGER,
  })
  @ForeignKey(() => FormFieldEntity)
  formFieldId: string;
  
  @Column({
    type: DataType.STRING,
    allowNull: false,
  })
  value: string;

}
