import { HttpStatus } from '@nestjs/common';
import { iResponseStatusMessage } from 'src/utils/response/response.interface';

export const responseName = {
    DATA_CREATED: 'DATA_CREATED',
    GET_FORM: 'GET_FORM',
    FORM_CREATED: 'FORM_CREATED',
};

export const responseInfo: Record<string, iResponseStatusMessage> = {
    DATA_CREATED: {
        message: 'Data created successfully',
        statusCode: HttpStatus.CREATED,
    },
    FORM_CREATED: {
        message: 'FORM created successfully',
        statusCode: HttpStatus.CREATED,
    },
    GET_FORM: {
        message: 'Form Fetched successfully',
        statusCode: HttpStatus.OK,
    },
};
